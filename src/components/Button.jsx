import React from 'react';

function Button({ onClickHandler, buttonText }) {
  return <button onClick={onClickHandler}>{buttonText}</button>;
}

export default Button;
