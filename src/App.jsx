import React, { useState } from 'react';
import Button from './components/Button';

function App() {
  const [count, setCount] = useState(0);
  const [color, setColor] = useState('green');
  const [song, setSong] = useState({ name: 'abcd', duration: 15 });
  const [dog, setDog] = useState(null);

  return (
    <>
      <h1 style={{ color }}>Count: {count}</h1>

      {/* document.getElement("button").addEventListener("click", fn) */}
      <Button
        onClickHandler={() => {
          setCount(count + 1);
        }}
        buttonText='Increment'
      />
      <Button
        onClickHandler={() => {
          setSong({ name: 'abcd', duration: song.duration + 5 });
        }}
        buttonText='Update duration'
      />
      <Button
        onClickHandler={() => {
          fetch('https://dog.ceo/api/breeds/image/random')
            .then((res) => {
              if (res.ok) {
                return res.json();
              }
            })
            .then((data) => {
              setDog(data);
            });
        }}
        buttonText='Get Dog'
      />
      <img
        src={
          dog?.message ??
          'https://images.dog.ceo/breeds/spaniel-welsh/n02102177_48.jpg'
        }
      />
      <div>{JSON.stringify(dog, null, 2)}</div>
    </>
  );
}

// Don't do this
// module.exports = App
export default App;

// Create the element (HTML) X
// Select the element (JS) X
// Modify the value -> addEventListener("click", fn) X
// Get the value (JS) X
// textContent = value (JS) X
// Display to the screen (HTML)

// -> React maintains the virtual structure of current state -> 0
// -> When we click the button -> setCount(count + 1)
// -> We get a complete new representation for count
// -> React performs a diff with the actual DOM
// -> And the since the value updated by 1, it will perform a re-render
